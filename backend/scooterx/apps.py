from django.apps import AppConfig


class ScooterxConfig(AppConfig):
    name = 'scooterx'
