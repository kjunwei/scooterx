from rest_framework import serializers
from .models import Region, Scooter


class ScooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scooter
        fields = ('id', 'longitude', 'latitude', 'region')

    def validate(self, attrs):
        longitude = attrs["longitude"]
        latitude = attrs["latitude"]
        region = attrs["region"]
        errors = []
        if longitude < region.minLongitude or longitude > region.maxLongitude:
            errors.append('The longitude must be within range of region of ' +
                          str(region.minLongitude) + " to " + str(region.maxLongitude))
        if latitude < region.minLatitude or latitude > region.maxLatitude:
            errors.append('The latitude must be within range of region of ' +
                          str(region.minLatitude) + " to " + str(region.maxLatitude))
        if len(errors) != 0:
            raise serializers.ValidationError(errors)
        return attrs


class RegionSerializer(serializers.ModelSerializer):
    scooters = ScooterSerializer(many=True, read_only=True)

    class Meta:
        model = Region
        fields = ('id', 'name', 'minLatitude', 'maxLatitude', 'minLongitude', 'maxLongitude', 'scooters')

    def validate(self, attrs):
        minLatitude = attrs["minLatitude"]
        maxLatitude = attrs["maxLatitude"]
        minLongitude = attrs["minLongitude"]
        maxLongitude = attrs["maxLongitude"]
        errors = []
        if self.instance is not None:
            scooters = Scooter.objects.filter(region__exact=self.instance.id)
            for scooter in scooters:
                if (scooter.longitude < minLongitude or scooter.longitude > maxLongitude) or \
                        (scooter.latitude < minLatitude or scooter.latitude > maxLatitude):
                    errors.append('Some scooters in the region does not satisfy the updated constraints.')
                    break

        if minLatitude >= maxLatitude:
            errors.append('The min latitude must be less than the max latitude.')
        elif minLongitude >= maxLongitude:
            errors.append('The min longitude must be less than the max longitude.')

        if len(errors) != 0:
            raise serializers.ValidationError(errors)
        return attrs
