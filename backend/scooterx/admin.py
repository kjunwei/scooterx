from django.contrib import admin
from .models import Region
from .models import Scooter

admin.site.register(Region)
admin.site.register(Scooter)