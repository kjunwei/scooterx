from math import radians, sin, cos, asin, sqrt

from django.db import models


class Region(models.Model):
    name = models.CharField(unique=True, max_length=120)
    minLatitude = models.FloatField()
    maxLatitude = models.FloatField()
    minLongitude = models.FloatField()
    maxLongitude = models.FloatField()

    def __str__(self):
        return self.name


class Scooter(models.Model):
    region = models.ForeignKey(Region, related_name="scooters", null=True, on_delete=models.CASCADE)
    longitude = models.FloatField()
    latitude = models.FloatField()

    def distanceTo(self, lat2, lon2):
        lon1 = radians(self.longitude)
        lon2 = radians(lon2)
        lat1 = radians(self.latitude)
        lat2 = radians(lat2)

        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371
        # calculate the result
        return c * r
