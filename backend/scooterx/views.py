import random
from math import radians, cos

from django.db.models import F, ExpressionWrapper, FloatField
from django.db.models.functions import Radians, Sin, Cos, Sqrt, ASin
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from util.util import isfloat
from .serializers import RegionSerializer, ScooterSerializer
from .models import Region, Scooter


class RegionView(viewsets.ModelViewSet):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()

    @staticmethod
    @api_view(['POST'])
    def generate_scooters(request, region_id):
        num_of_scooters = 10
        region = get_object_or_404(Region, pk=region_id)
        for i in range(num_of_scooters):
            latitude = random.uniform(region.minLatitude, region.maxLatitude)
            longitude = random.uniform(region.minLongitude, region.maxLongitude)
            scooter = Scooter(region=region, longitude=longitude, latitude=latitude)
            scooter.save()
        return Response(status=200, data=RegionSerializer(Region.objects.get(pk=region_id)).data)


class ScooterView(viewsets.ModelViewSet):
    serializer_class = ScooterSerializer
    queryset = Scooter.objects.all()

    def get_queryset(self):
        queryset = Scooter.objects.all()
        region_id = self.request.query_params.get('region', None)
        latitude = self.request.query_params.get('latitude', None)
        longitude = self.request.query_params.get('longitude', None)
        range = self.request.query_params.get('range', None)
        limit = self.request.query_params.get('limit', None)
        print(limit)

        if region_id is not None:
            queryset.filter(region__exact=region_id)
        if latitude is not None and longitude is not None and isfloat(latitude) and isfloat(longitude):
            latitude = float(latitude)
            longitude = float(longitude)

            queryset = queryset.annotate(longitude_r=Radians('longitude'))
            queryset = queryset.annotate(latitude_r=Radians('latitude'))
            latitude = radians(latitude)
            longitude = radians(longitude)

            # Haversine formula
            dlonF = (longitude - F('longitude_r')) / 2
            dlatF = (latitude - F('latitude_r')) / 2
            queryset = queryset.annotate(dlon=Sin(ExpressionWrapper(dlonF, output_field=FloatField())))
            queryset = queryset.annotate(dlat=Sin(ExpressionWrapper(dlatF, output_field=FloatField())))
            queryset = queryset.annotate(cos_lat=Cos('latitude_r'))
            aFunc = (F('dlat') ** 2) + F('cos_lat') * cos(latitude) * (F('dlon') ** 2)
            queryset = queryset.annotate(a=ExpressionWrapper(aFunc, output_field=FloatField()))
            queryset = queryset.annotate(c=ASin(Sqrt('a')))
            resultFunc = 2 * F('c') * 6371
            queryset = queryset.annotate(distance_to_point=ExpressionWrapper(resultFunc, output_field=FloatField()))
            queryset = queryset.order_by('distance_to_point')

            if range is not None:
                diffFunc = F('distance_to_point') - range
                # print(queryset.count())
                queryset = queryset.annotate(diff_range=ExpressionWrapper(diffFunc, output_field=FloatField()))\
                    .filter(diff_range__lt=0)
                # print(queryset.count(), queryset.first().diff_range)

        if limit is not None and limit.isdigit():
            limit = int(limit)
            queryset = queryset[:limit]

        return queryset




