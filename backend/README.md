# Backend

## Instructions

1. Install python 3 and pipenv into local machine
2. `pipenv install`
3. `python manage.py migrate`
4. `python manage.py runserver`
5. Go to http://127.0.0.1:8000/api/regions/ to add a region and their respective bounding area.
    - I used the values from here: https://gist.github.com/graydon/11198540
6. Go to frontend to generate scooters!
