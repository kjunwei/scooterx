from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from scooterx import views

router = routers.DefaultRouter()
router.register(r'regions', views.RegionView, 'regions')
router.register(r'scooters', views.ScooterView, 'scooters')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/regions/<slug:region_id>/generate_scooters/', views.RegionView.generate_scooters, name='generate_scooters'),
    path('api/', include(router.urls))
]
