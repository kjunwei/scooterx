import {RegionsAPI} from "./regions";
import {ScootersAPI} from "./scooters";

class API {
  region = new RegionsAPI();
  scooter = new ScootersAPI();
}

const api = new API();

export default api;
