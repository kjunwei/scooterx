import axios from "axios";

class BaseAPI {
  constructor() {
    const headers = { Accept: "application/json" };
    const params = { format: "json" };

    this.client = axios.create({
      baseURL: "http://127.0.0.1:8000/api/",
      timeout: 20000,
      headers,
      params
    });
  }

  getClient() {
    return this.client;
  }
}

export default BaseAPI;
