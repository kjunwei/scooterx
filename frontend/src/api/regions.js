import BaseAPI from "./base";

export class RegionsAPI extends BaseAPI {
  list() {
    return this.getClient().get(`${this.getUrl()}/`);
  }

  get(id) {
    return this.getClient().get(`${this.getUrl()}/${id}/`)
  }

  generateScooters(regionId) {
    return this.getClient().post(`${this.getUrl()}/${regionId}/generate_scooters/`);
  }

  getUrl() {
    return "/regions";
  }
}
