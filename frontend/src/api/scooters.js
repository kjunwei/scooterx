import BaseAPI from "./base";

export class ScootersAPI extends BaseAPI {
  list(regionId, longitude, latitude, range, limit) {
    let queryArr = ["region=" + regionId, "longitude=" + longitude, "latitude=" + latitude, "range=" + range,
      "limit=" + limit];
    let query = "?" + queryArr.join("&");
    return this.getClient().get(`${this.getUrl()}/${query}`);
  }

  getUrl() {
    return "/scooters";
  }
}
