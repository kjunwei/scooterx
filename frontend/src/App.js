import React, {useEffect, useState} from 'react';
import './App.css';
import api from "./api";
import {Jumbotron, Form, FormGroup, Label, Container, Row, Col, Input} from "reactstrap/es";
import GoogleMapReact from 'google-map-react';
import MapMarker from "./MapMarker";
import Button from "reactstrap/es/Button";
import FormText from "reactstrap/es/FormText";

function App() {
  const center = {lat: 1.34, lng: 103.808053 };
  const zoom = 12;
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState();

  const [longitude, setLongitude] = useState();
  const [latitude, setLatitude] = useState();
  const [range, setRange] = useState();
  const [numScooters, setNumScooters] = useState();

  const [delayTimer, setDelayTime] = useState();

  useEffect(() => {
    api.region.list().then(response => {
      setCountries(response.data);
      if (response.data && response.data.length !== 0) {
        setSelectedCountry(response.data[0]);
      }
    })
  }, []);

  const changeRegion = (e) => {
    api.region.get(e.target.value).then(response => {
      setSelectedCountry(response.data);
    })
  };


  useEffect(() => {
    if (!selectedCountry) {
      return;
    }
    clearTimeout(delayTimer);
    setDelayTime(setTimeout(() => {
      api.scooter.list(selectedCountry.id, longitude, latitude, range, numScooters)
        .then(response => {
          setSelectedCountry({
            ...selectedCountry,
            scooters: response.data
          });
        })
        .catch(error => {});
    }, 1000));
  }, [longitude, latitude, range, numScooters]);

  const generateMoreScooters = () => {
    if (selectedCountry) {
      api.region.generateScooters(selectedCountry.id).then(response => {
        api.scooter.list(selectedCountry.id, longitude, latitude, range, numScooters)
          .then(response => {
            setSelectedCountry({
              ...selectedCountry,
              scooters: response.data
            });
          });
      });
    }
  };

  return (
    <div className={"content-wrap"}>
      <Jumbotron>
        <h1 className={"center-text"}>ScooterX</h1>
        <p className={"center-text"}>Find nearby scooters by entering the following parameters!</p>
      </Jumbotron>

      <Container>
        <Form>
          <FormGroup>
            <Row>
              <Col>
                <Label for="countrySelect">Country</Label>
                <Input onChange={changeRegion} type="select" name="countrySelect" id="countrySelect">
                  {countries.map((country) =>
                    <option key={country.id} value={country.id}>{country.name}</option>
                  )}
                </Input>
              </Col>
              <Col className={"align-self-center"} style={{textAlign: "right"}}>
                <Button color="primary" onClick={generateMoreScooters}>Generate More Scooters</Button>
              </Col>
            </Row>
          </FormGroup>

          <FormGroup>
            <Row>
              <Col>
                <Label for={"inputLatitude"}>Latitude</Label>
                <Input onChange={(e) => setLatitude(e.target.value)} type="number"
                       name="latitude" id="inputLatitude" />
                {selectedCountry && (
                  <FormText color="muted">
                    MIN: {selectedCountry.minLatitude} <br/> MAX: {selectedCountry.maxLatitude}
                  </FormText>
                )}
              </Col>
              <Col>
                <Label for={"inputLongitude"}>Longitude</Label>
                <Input onChange={(e) => setLongitude(e.target.value)}
                       type="number" name="longitude" id="inputLongitude" />
                {selectedCountry && (
                  <FormText color="muted">
                    MIN: {selectedCountry.minLongitude} <br/> MAX: {selectedCountry.maxLongitude}
                  </FormText>
                )}
              </Col>

              <Col>
                <Label for={"range"}>Range (km)</Label>
                <Input onChange={(e) => setRange(e.target.value)}
                       type="number" name="range" id="range" />
              </Col>

              <Col>
                <Label for={"limit"}>Num Of Scooters</Label>
                <Input onChange={(e) => setNumScooters(e.target.value)}
                       type="number" name="limit" id="limit" />
              </Col>
            </Row>
          </FormGroup>
        </Form>
      </Container>

      <div className={"container content-fill"}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_MAP_API_KEY }}
          defaultCenter={center}
          defaultZoom={zoom}
        >
          {selectedCountry && selectedCountry.scooters.map((scooter) =>
            <MapMarker key={scooter.id} lat={scooter.latitude} lng={scooter.longitude} color={"green"}/>
          )}

          <MapMarker lat={latitude} lng={longitude} color={"blue"}/>
        </GoogleMapReact>
      </div>

      <div className={"footer"}>

      </div>
    </div>
  );
}

export default App;
